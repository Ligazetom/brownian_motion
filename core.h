#ifndef CORE_H
#define CORE_H

#define GRAPH_WIDTH_MARGIN_LEFT 100
#define GRAPH_WIDTH_MARGIN_RIGHT 40
#define GRAPH_HEIGHT_MARGIN_TOP 40
#define GRAPH_HEIGHT_MARGIN_BOT 60
#define X_AXIS_DIVISION 5
#define Y_AXIS_DIVISION 8
#define X_BARS_HALF_HEIGHT 5
#define Y_BARS_HALF_WIDTH 5
#define X_AXIS_VECTOR_DIVISIONS 6
#define Y_AXIS_VECTOR_DIVISIONS 6

#include "qimage.h"
#include "qpainter.h"
#include "bmath.h"
#include <vector>
#include "qpoint.h"
#include "qdebug.h"

class Core
{
public:
    Core();
    void newImage(int x, int y);
    QImage* generateTrajectoryImage(int trajAmount, const std::vector<double> &CoordsX);
    QImage* getRandomVectorImage();
    void setActiveTrajectoryImage(QImage *img);
    void setActiveVectorImage(QImage *img);
    void setSuitableLast(bool b);
    void setRandomVectorCoordsId(int id);

private:
    QImage *img;
    QImage *vectorImg;
    QPoint zero;
    QRect graphEdge;
    int suitableTrajectoryCount;
    int randomVectorCoordsId;
    bool suitableLast;
    double min, max;
    double vectorCoordsMin1, vectorCoordsMax1, vectorCoordsMin2, vectorCoordsMax2;
    std::vector<int> scaledCoordsX;
    std::vector<std::vector<double>> brownTrajectories;
    std::vector<int> suitableTrajIndex;

    void checkRandomVectorCoordsValuesMaxMin(int trajIndex);
    void generateBrownianTrajectories(int trajAmount, const std::vector<double> &CoordsX);
    std::vector<double> generateBrownianTrajectory(const std::vector<double> &CoordsX);
    void drawBrownianTrajectory(int trajIndex, QColor color);
    void drawBrownianTrajectories();
    void drawAxes(const std::vector<double> &CoordsX);
    bool isSuitable(int trajIndex) const;
    void drawSuitableTrajectories();
    void drawTrajectoryToImage(const QPen &pen, int trajIndex);
    void scaleTrajectoryCoordsY(int trajIndex);
    void scaleTrajectoryCoordsX(const std::vector<double> &CoordsX);
    void setDrawClipping(int endPoint);
    void drawResultsInfo(int trajAmount);
    void drawRandomVectorGraph();
    void drawIntervals(QPainter *painter, double endPoint);
    int scaleValue(int minWanted, int maxWanted, double max, double min, double value);
    void drawRandomVectorAxes();
    void drawRandomVectorPoints();
    void drawSuitableVectorPoints();
    QPoint whichRandomVectorCoords();
    void drawRandomVectorIntervals(QPainter *painter);
    void drawRandomVectorGraphInfo();
    void drawAxisBars(QPainter *painter, int divisionsX, int divisionsY, bool skipMid);
    void drawAxisValues(QPainter *painter, QPointF maxMin1, QPointF maxMin2, int divisionsX, int divisionsY, bool skipEdgeValues);
    void drawAxisNames(QPainter *painter, QString axX, QString axY);
};

#endif // CORE_H
