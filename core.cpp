#include "core.h"

Core::Core(){
    max = 0.;
    min = 0.;
    randomVectorCoordsId = 0;
}

void Core::newImage(int x, int y){
    img = new QImage(x, y, QImage::Format_RGB32);
    img->fill(Qt::white);
    vectorImg = new QImage(x, y, QImage::Format_RGB32);
    vectorImg->fill(Qt::white);
    zero = QPoint(GRAPH_WIDTH_MARGIN_LEFT, y / 2);
    suitableLast = false;
}

QImage* Core::generateTrajectoryImage(int trajAmount, const std::vector<double> &CoordsX){
    suitableTrajectoryCount = 0;
    suitableTrajIndex.clear();
    img->fill(Qt::white);

    scaleTrajectoryCoordsX(CoordsX);
    setDrawClipping(scaledCoordsX[scaledCoordsX.size() - 1]);
    generateBrownianTrajectories(trajAmount, CoordsX);
    drawRandomVectorGraph();
    drawBrownianTrajectories();

    if(suitableLast){
        drawSuitableTrajectories();
    }

    drawAxes(CoordsX);
    drawResultsInfo(trajAmount);

    return img;
}

void Core::drawRandomVectorGraph(){
    vectorImg->fill(Qt::white);

    drawRandomVectorAxes();
    drawRandomVectorPoints();

    if(suitableLast){
        drawSuitableVectorPoints();
    }

    drawRandomVectorGraphInfo();
}

void Core::drawRandomVectorAxes(){
    QPainter painter(vectorImg);
    QPen pen;
    QFont font;

    font.setBold(true);
    painter.setFont(font);
    pen.setColor(QColor(30,30,30));
    pen.setWidth(2);
    painter.setPen(pen);

    painter.drawRect(graphEdge);
    drawRandomVectorIntervals(&painter);

    font.setPixelSize(18);

    painter.setFont(font);

    drawAxisNames(&painter, QString("B(%1)").arg(whichRandomVectorCoords().x() + 1), QString("B(%1)").arg(whichRandomVectorCoords().y() + 1));
}

void Core::drawAxisBars(QPainter *painter, int divisionsX, int divisionsY, bool skipMid){
    int graphStepX = (graphEdge.right() - graphEdge.left()) / divisionsX;
    int graphStepY = (graphEdge.bottom() - graphEdge.top()) / divisionsY;

    for(int i = 1; i < divisionsX; i++){
        painter->drawLine(QPoint(zero.x() + round(i * graphStepX), graphEdge.bottomRight().y() - X_BARS_HALF_HEIGHT),
                         QPoint(zero.x() + round(i * graphStepX), graphEdge.bottomRight().y())
                         );
    }

    for(int i = 1; i < divisionsY; i++){
        if(i == divisionsY / 2 && skipMid){
            continue;
        }

        painter->drawLine(QPoint(graphEdge.left(), graphEdge.top() + round(i * graphStepY)),
                          QPoint(graphEdge.left() + Y_BARS_HALF_WIDTH, graphEdge.top() + round(i * graphStepY))
                          );

    }
}

void Core::drawRandomVectorIntervals(QPainter *painter){
    QPointF maxMin1(vectorCoordsMax1, vectorCoordsMin1);
    QPointF maxMin2(vectorCoordsMax2, vectorCoordsMin2);

    drawAxisBars(painter, X_AXIS_VECTOR_DIVISIONS, Y_AXIS_VECTOR_DIVISIONS, false);
    drawAxisValues(painter, maxMin1, maxMin2, X_AXIS_VECTOR_DIVISIONS, Y_AXIS_VECTOR_DIVISIONS, false);
}

void Core::drawRandomVectorGraphInfo(){
    QPainter painter(vectorImg);
    QPen pen;
    QFont font;

    pen.setColor(QColor(30,30,30));
    font.setBold(true);
    font.setPixelSize(18);
    painter.setFont(font);
    painter.setPen(pen);

    painter.drawText(QRect(
                         QPoint(zero.x(), 0),
                         QPoint(zero.x() + (graphEdge.right() - graphEdge.left()), GRAPH_HEIGHT_MARGIN_TOP)
                         ),
                     Qt::AlignCenter,
                     QString("Random vector (B%1, B%2) - N(%3)").arg(whichRandomVectorCoords().x() + 1).arg(whichRandomVectorCoords().y() + 1).arg(brownTrajectories.size())
                     );
}

void Core::drawSuitableVectorPoints(){
    QPainter painter(vectorImg);
    QBrush brush;
    QPen pen;
    QPoint coords = whichRandomVectorCoords();

    brush.setColor(QColor(201,40,42));
    brush.setStyle(Qt::SolidPattern);
    pen.setColor(QColor(201,40,42));
    painter.setPen(pen);
    painter.setBrush(brush);

    for(unsigned int i = 0; i < suitableTrajIndex.size(); i++){
        painter.drawEllipse(QPoint(
                                scaleValue(graphEdge.left(), graphEdge.right(), vectorCoordsMax1, vectorCoordsMin1, brownTrajectories[suitableTrajIndex[i]][coords.x()]),
                                scaleValue(graphEdge.bottom(), graphEdge.top(), vectorCoordsMax2, vectorCoordsMin2, brownTrajectories[suitableTrajIndex[i]][coords.y()])
                                ),
                            4,
                            4
                            );
    }
}

void Core::drawRandomVectorPoints(){
    QPainter painter(vectorImg);
    QBrush brush;
    QPen pen;
    QPoint coords = whichRandomVectorCoords();

    brush.setStyle(Qt::SolidPattern);

    for(int j = 1, i = brownTrajectories.size() - 1; i >= 0; i--){
        if((unsigned int)j <= suitableTrajIndex.size()){
            if(i == suitableTrajIndex[suitableTrajIndex.size() - j]){
                j++;

                if(suitableLast){
                    continue;
                }

                brush.setColor(QColor(201,40,42));
                pen.setColor(QColor(201,40,42));
                painter.setPen(pen);
                painter.setBrush(brush);

                painter.drawEllipse(QPoint(
                                        scaleValue(graphEdge.left(), graphEdge.right(), vectorCoordsMax1, vectorCoordsMin1, brownTrajectories[i][coords.x()]),
                                        scaleValue(graphEdge.bottom(), graphEdge.top(), vectorCoordsMax2, vectorCoordsMin2, brownTrajectories[i][coords.y()])
                                        ),
                                    4,
                                    4
                                    );
                continue;
            }
        }
        pen.setColor(QColor(201,153,40));
        brush.setColor(QColor(201,153,40));
        painter.setBrush(brush);
        painter.setPen(pen);

        painter.drawEllipse(QPoint(
                                scaleValue(graphEdge.left(), graphEdge.right(), vectorCoordsMax1, vectorCoordsMin1, brownTrajectories[i][coords.x()]),
                                scaleValue(graphEdge.bottom(), graphEdge.top(), vectorCoordsMax2, vectorCoordsMin2, brownTrajectories[i][coords.y()])
                            ),
                            4,
                            4
                            );
    }
}

void Core::drawBrownianTrajectories(){
    for(int j = 1, i = brownTrajectories.size() - 1; i >= 0; i--){
        if((unsigned int)j <= suitableTrajIndex.size()){
            if(i == suitableTrajIndex[suitableTrajIndex.size() - j]){
                j++;

                if(suitableLast){
                    continue;
                }

                drawBrownianTrajectory(i, QColor(201,40,42));
                continue;
            }
        }
        drawBrownianTrajectory(i, QColor(201,153,40));
    }
}

void Core::drawSuitableTrajectories(){
    for(unsigned int i = 0; i < suitableTrajIndex.size(); i++){
        drawBrownianTrajectory(suitableTrajIndex[i], QColor(201,40,42));
    }
}

void Core::drawBrownianTrajectory(int trajIndex, QColor color){
    QPen pen;    
    pen.setColor(color);

    scaleTrajectoryCoordsY(trajIndex);
    drawTrajectoryToImage(pen, trajIndex);
}

void Core::generateBrownianTrajectories(int trajAmount, const std::vector<double> &CoordsX){
    brownTrajectories.clear();
    brownTrajectories.reserve(trajAmount);
    max = 0.;
    min = 0.;
    vectorCoordsMax1 = 0.;
    vectorCoordsMin1 = 0.;
    vectorCoordsMax2 = 0.;
    vectorCoordsMin2 = 0.;

    for(int i = 0; i < trajAmount; i++){
        brownTrajectories.emplace_back(generateBrownianTrajectory(CoordsX));
        checkRandomVectorCoordsValuesMaxMin(i);

        if(isSuitable(i)){
            suitableTrajectoryCount++;
            suitableTrajIndex.push_back(i);
        }
    }
}

QPoint Core::whichRandomVectorCoords(){
    int coord1, coord2;

    switch(randomVectorCoordsId){
    case 0:{
        coord1 = 0;
        coord2 = 1;
        break;
    }
    case 1:{
        coord1 = 0;
        coord2 = 2;
        break;
    }
    case 2:{
        coord1 = 1;
        coord2 = 2;
        break;
    }
    default:{
        coord1 = 0;
        coord2 = 1;
    }
    }

    return QPoint(coord1, coord2);
}

void Core::checkRandomVectorCoordsValuesMaxMin(int trajIndex){
    QPoint coords = whichRandomVectorCoords();


    if(brownTrajectories[trajIndex][coords.x()] > vectorCoordsMax1){
        vectorCoordsMax1 = brownTrajectories[trajIndex][coords.x()];
    }

    if(brownTrajectories[trajIndex][coords.x()] < vectorCoordsMin1){
        vectorCoordsMin1 = brownTrajectories[trajIndex][coords.x()];
    }

    if(brownTrajectories[trajIndex][coords.y()] > vectorCoordsMax2){
        vectorCoordsMax2 = brownTrajectories[trajIndex][coords.y()];
    }

    if(brownTrajectories[trajIndex][coords.y()] < vectorCoordsMin2){
        vectorCoordsMin2 = brownTrajectories[trajIndex][coords.y()];
    }

    fabs(vectorCoordsMax1) > fabs(vectorCoordsMin1) ? vectorCoordsMin1 = -vectorCoordsMax1 : vectorCoordsMax1 = -vectorCoordsMin1;
    fabs(vectorCoordsMax2) > fabs(vectorCoordsMin2) ? vectorCoordsMin2 = -vectorCoordsMax2 : vectorCoordsMax2 = -vectorCoordsMin2;
}

std::vector<double> Core::generateBrownianTrajectory(const std::vector<double> &CoordsX){
    std::vector<double> trajBuff;
    double buff = 0.;

    trajBuff.reserve(CoordsX.size());

    for(unsigned int i = 0; i < CoordsX.size(); i++){
        buff += BMath::generateNormalDistributedNumber();

        if(buff > max){
            max = buff;
        }
        if(buff < min){
            min = buff;
        }

       trajBuff.emplace_back(buff);
    }

    fabs(max) > fabs(min) ? min = -max : max = -min;            //symetria grafu

    return trajBuff;
}

bool Core::isSuitable(int trajIndex) const{
    const double b1 = brownTrajectories[trajIndex][0];
    const double b2 = brownTrajectories[trajIndex][1];
    const double b3 = brownTrajectories[trajIndex][2];

    if(b1 < 0 && b2 > b1 && b3 > b2 && b1 + b3 < b2){
        return true;
    }

    return false;
}

void Core::drawAxes(const std::vector<double> &CoordsX){
    QPainter painter(img);
    QPen pen;
    QFont font;

    font.setBold(true);;
    font.setPixelSize(12);
    pen.setColor(QColor(30,30,30));
    pen.setWidth(2);

    painter.setFont(font);
    painter.setPen(pen);
    painter.drawRect(graphEdge);

    drawIntervals(&painter, *(CoordsX.end() - 1));

    font.setPixelSize(18);
    painter.setFont(font);

    drawAxisNames(&painter, QString("t"), QString("B(t)"));
}

void Core::drawAxisNames(QPainter *painter, QString axX, QString axY){
    painter->drawText(QRect(
                         QPoint(zero.x(), img->height() - GRAPH_HEIGHT_MARGIN_BOT),
                         QPoint(zero.x() + (graphEdge.right() - graphEdge.left()), img->height())
                         ),
                     Qt::AlignCenter,
                     axX
                     );

    painter->translate(30, zero.y());
    painter->rotate(-90);
    painter->translate(-30, -zero.y());
    painter->drawText(QRect(
                         QPoint(0, zero.y() - 50),
                         QPoint(70, zero.y() + 40)
                         ),
                     Qt::AlignCenter,
                     axY
                     );
}

void Core::scaleTrajectoryCoordsX(const std::vector<double> &CoordsX){
    double coef = (img->width() - GRAPH_WIDTH_MARGIN_LEFT - GRAPH_WIDTH_MARGIN_RIGHT) / *(CoordsX.end() - 1);
    scaledCoordsX.clear();
    scaledCoordsX.reserve(CoordsX.size());

    for(unsigned int i = 0; i < CoordsX.size(); i++){
        scaledCoordsX.emplace_back(round(CoordsX[i] * coef + zero.x()));
    }
}

void Core::scaleTrajectoryCoordsY(int trajIndex){
    int size = brownTrajectories[trajIndex].size();
    double coef = (graphEdge.top() - graphEdge.bottom()) / (max - min);

    for(int i = 0; i < size; i++){
        brownTrajectories[trajIndex][i] = round((brownTrajectories[trajIndex][i] - min) * coef + graphEdge.bottom());
    }
}

void Core::drawTrajectoryToImage(const QPen &pen, int trajIndex){
    QPainter painter(img);
    painter.setClipRect(graphEdge);
    painter.setPen(pen);
    painter.setRenderHint(painter.Antialiasing);

    painter.drawLine(zero, QPoint(scaledCoordsX[0], brownTrajectories[trajIndex][0]));

    for(unsigned int i = 1; i < scaledCoordsX.size(); i++){
        painter.drawLine(QPoint(scaledCoordsX[i - 1], brownTrajectories[trajIndex][i - 1]), QPoint(scaledCoordsX[i], brownTrajectories[trajIndex][i]));
        painter.drawEllipse(QPoint(scaledCoordsX[i], brownTrajectories[trajIndex][i]), 1, 1);
    }
}

void Core::setDrawClipping(int endPoint){
    graphEdge = QRect(QPoint(zero.x(), zero.y() - img->height() / 2 + GRAPH_HEIGHT_MARGIN_TOP),
                      QPoint(endPoint, zero.y() + img->height() / 2 - GRAPH_HEIGHT_MARGIN_BOT)
                      );
}

void Core::drawResultsInfo(int trajAmount){
    QPainter painter(img);
    QPen pen;
    QBrush brush;
    QFont font;
    double prob = suitableTrajectoryCount / (double)(trajAmount);

    brush.setStyle(Qt::SolidPattern);
    pen.setColor(QColor(30,30,30));
    font.setBold(true);
    font.setPixelSize(18);
    painter.setFont(font);
    painter.setPen(pen);

    painter.drawText(QRect(
                         QPoint(zero.x(), 0),
                         QPoint(zero.x() + (graphEdge.right() - graphEdge.left()), GRAPH_HEIGHT_MARGIN_TOP)
                         ),
                     Qt::AlignCenter,
                     QString("Brownian motion trajectories - N(%1)").arg(trajAmount)
                     );

    font.setPixelSize(12);
    painter.setFont(font);

    painter.drawText(QRect(
                         QPoint(zero.x() + 40, graphEdge.top() + 20),
                         QPoint(zero.x() + 150, graphEdge.top() + 40)
                         ),
                     Qt::AlignCenter,
                     QString("OK (%1)").arg(suitableTrajectoryCount)
                     );

    painter.drawText(QRect(
                         QPoint(zero.x() + 40, graphEdge.top() + 50),
                         QPoint(zero.x() + 150, graphEdge.top() + 70)
                         ),
                     Qt::AlignCenter,
                     QString("NOK (%1)").arg(trajAmount - suitableTrajectoryCount)
                     );

    font.setPixelSize(16);
    painter.setFont(font);

    painter.drawText(QRect(
                         QPoint(zero.x(), graphEdge.bottom() - 70),
                         QPoint((graphEdge.right() - graphEdge.left()) / 2, graphEdge.bottom())
                         ),
                     Qt::AlignCenter,
                     QString("Experimental probability: %1").arg(prob, 6, 'f', 4)
                     );


    brush.setColor(QColor(201,40,42));     //OK
    painter.setBrush(brush);
    painter.drawRect(QRect(
                         QPoint(zero.x() + 20, graphEdge.top() + 20),
                         QPoint(zero.x() + 40, graphEdge.top() + 40)
                         )
                     );



    brush.setColor(QColor(201,153,40));      //NOK
    painter.setBrush(brush);
    painter.drawRect(QRect(
                         QPoint(zero.x() + 20, graphEdge.top() + 50),
                         QPoint(zero.x() + 40, graphEdge.top() + 70)
                         )
                     );

}

void Core::drawAxisValues(QPainter *painter, QPointF maxMin1, QPointF maxMin2, int divisionsX, int divisionsY, bool skipEdgeValues){
    int graphStepX = (graphEdge.right() - graphEdge.left()) / divisionsX;
    int graphStepY = (graphEdge.bottom() - graphEdge.top()) / divisionsY;
    double stepX = (maxMin1.x() - maxMin1.y()) / (double)divisionsX;
    double stepY = (maxMin2.x() - maxMin2.y()) / (double)divisionsY;

    for(int i = 0; i <= divisionsX; i++){
        painter->drawText(QRect(
                              QPoint(zero.x() + round(i * graphStepX) - 40, graphEdge.bottomRight().y() + X_BARS_HALF_HEIGHT),
                              QPoint(zero.x() + round(i * graphStepX) + 40, graphEdge.bottomRight().y() + 4 * X_BARS_HALF_HEIGHT)
                              ),
                          Qt::AlignCenter,
                          QString("%1").arg(maxMin1.y() + stepX * i, 6, 'f', 4)
                          );
    }

    for(int i = 0; i <= divisionsY; i++){
        if((i == 0 && skipEdgeValues) || (i == divisionsY && skipEdgeValues)){
            continue;
        }

        painter->drawText(QRect(
                              QPoint(graphEdge.left() - 70, graphEdge.top() + round(i * graphStepY) - X_BARS_HALF_HEIGHT),
                              QPoint(graphEdge.left(), graphEdge.top() + round(i * graphStepY) + 2 * X_BARS_HALF_HEIGHT)
                              ),
                          Qt::AlignCenter,
                          QString("%1").arg(maxMin2.x() - stepY * i, 5, 'f', 3)
                          );
    }
}

void Core::drawIntervals(QPainter *painter, double endPoint){
    QPointF maxMin1(endPoint, 0);
    QPointF maxMin2(max, min);

    drawAxisBars(painter, X_AXIS_DIVISION, Y_AXIS_DIVISION, true);
    drawAxisValues(painter, maxMin1, maxMin2, X_AXIS_DIVISION, Y_AXIS_DIVISION, true);
}

void Core::setActiveTrajectoryImage(QImage *img){
    this->img = img;
}

void Core::setActiveVectorImage(QImage *img){
    vectorImg = img;
}

QImage* Core::getRandomVectorImage(){
    return vectorImg;
}

void Core::setSuitableLast(bool b){
    suitableLast = b;
}

void Core::setRandomVectorCoordsId(int id){
    randomVectorCoordsId = id;
}

int Core::scaleValue(int minWanted, int maxWanted, double max, double min, double value){
    double coef = (maxWanted - minWanted) / (max - min);

    return round((value - min) * coef + minWanted);
}
