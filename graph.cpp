#include "graph.h"

Graph::Graph(QWidget *parent)
	: QWidget(parent) {
	setAttribute(Qt::WA_StaticContents);

    for(int i = 0; i < 3; i++){
        vectors[i] = nullptr;
        trajectories[i] = nullptr;
    }

    baseImage = QImage(100, 100, QImage::Format_ARGB32);
    baseImage.fill(Qt::white);
}

Graph::~Graph()
{
}

void Graph::setTrajectoryImage(QImage *img, int index) {
    trajectories[index] = img;
}

void Graph::setVectorImage(QImage *img, int index){
    vectors[index] = img;
}

void Graph::setActiveImage(bool isVector, int index){
    if(trajectories[index] == nullptr){
        activeImage = &baseImage;
        update();
        return;
    }

    isVector ? activeImage = vectors[index] : activeImage = trajectories[index];
    this->setMinimumSize(activeImage->size());
    update();
}

void Graph::paintEvent(QPaintEvent *event) {
    QPainter painter(this);
    QRect area = event->rect();
    painter.drawImage(area, *activeImage);
}

QImage* Graph::getTrajectoryImage(int index){
    return trajectories[index];
}

QImage* Graph::getVectorImage(int index){
    return vectors[index];
}
