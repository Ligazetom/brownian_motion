#include "bmath.h"

double BMath::generateNormalDistributedNumber(){
    double buff, sum = 0;

    for(int i = 0; i < CLT_AMOUNT_TO_GENERATE; i++){
            buff = rand() / (double)RAND_MAX;
            buff = (MAX_GEN_VAL - MIN_GEN_VAL) * buff + MIN_GEN_VAL;
            sum += buff;
    }

    return buff / sqrt(CLT_AMOUNT_TO_GENERATE);
}
