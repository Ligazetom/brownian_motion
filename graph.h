#pragma once

#include <QWidget>
#include <QImage>
#include <QRect>
#include <QPainter>
#include <QPaintEvent>

class Graph : public QWidget
{
	Q_OBJECT

public:
    Graph(QWidget *parent = Q_NULLPTR);
    ~Graph();

    void setTrajectoryImage(QImage *img, int index);
    void setVectorImage(QImage *img, int index);
    void setActiveImage(bool isVector, int index);
    QImage *getTrajectoryImage(int index);
    QImage *getVectorImage(int index);

protected:
    void paintEvent(QPaintEvent *event);

private:
    QImage baseImage;
    QImage *activeImage;
    QImage *trajectories[3];
    QImage *vectors[3];
};
