#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->scrollArea->setBackgroundRole(QPalette::Dark);
    ui->scrollArea->setWidgetResizable(false);
    ui->scrollArea->setWidget(&graph);

    for(int i = 0; i < 3; i++){
        shouldCreateNew[i] = true;
    }

    msg.setText("Wrong X values format.\nPositive values separated by commas only!");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButtonGenerate_pressed(){
    if(!areXValuesValid()){
        msg.show();
        return;
    }

    if(shouldCreateNew[ui->comboBoxImage->currentIndex()]){
        core.newImage(ui->scrollArea->width() - IMAGE_MARGIN, ui->scrollArea->height() - IMAGE_MARGIN);
        shouldCreateNew[ui->comboBoxImage->currentIndex()] = false;
    }
    else{
        core.setActiveTrajectoryImage(graph.getTrajectoryImage(ui->comboBoxImage->currentIndex()));
        core.setActiveVectorImage(graph.getVectorImage(ui->comboBoxImage->currentIndex()));
    }

    graph.setTrajectoryImage(core.generateTrajectoryImage(ui->spinBoxTrajAmount->value(), getXValues()), ui->comboBoxImage->currentIndex());
    graph.setVectorImage(core.getRandomVectorImage(), ui->comboBoxImage->currentIndex());
    graph.setActiveImage(isVectorImageWanted(), ui->comboBoxImage->currentIndex());
}

std::vector<double> MainWindow::getXValues(){
    std::vector<double> buff;
    QStringList list;

    buff.reserve(ui->spinBoxSteps->value());
    list = ui->lineEditXVals->text().trimmed().split(",");

    for(int i = 0; i < list.size(); i++){
        buff.emplace_back(list[i].toDouble());
    }

    return buff;
}

void MainWindow::on_lineEditXVals_editingFinished(){
    QStringList list;

    list = ui->lineEditXVals->text().trimmed().split(",");

    if(list.size() == 1 && list[0] == ""){
        list = QStringList();
        list.append(QString("%1").arg(ui->doubleSpinBoxEndpt->value()));
        ui->lineEditXVals->setText(list[0]);
    }

    ui->doubleSpinBoxEndpt->setValue(list[list.size() - 1].toDouble());
    ui->spinBoxSteps->setValue(list.size());
}



void MainWindow::on_spinBoxSteps_editingFinished(){
    generateXValues();
}

void MainWindow::on_doubleSpinBoxEndpt_editingFinished(){
    generateXValues();
}

void MainWindow::generateXValues(){
    QStringList list;
    int stepsCount = ui->spinBoxSteps->value();
    double step = ui->doubleSpinBoxEndpt->value() / stepsCount;

    list.reserve(stepsCount);

    for(int i = 1; i <= stepsCount; i++){
        list.append(QString("%1").arg(step * i));
    }

    ui->lineEditXVals->setText(list.join(","));
}

void MainWindow::on_comboBoxImage_currentIndexChanged(int i){
    graph.setActiveImage(isVectorImageWanted(), i);
}

void MainWindow::on_radioButtonTrajectory_toggled(){
    graph.setActiveImage(isVectorImageWanted(), ui->comboBoxImage->currentIndex());
}

void MainWindow::on_radioButtonVector_toggled(){
    graph.setActiveImage(isVectorImageWanted(), ui->comboBoxImage->currentIndex());
}

bool MainWindow::isVectorImageWanted(){
    if(ui->radioButtonVector->isChecked()){
        return true;
    }

    return false;
}

void MainWindow::on_checkBoxSuitable_toggled(bool b){
    core.setSuitableLast(b);
}

void MainWindow::on_comboBoxCoords_currentIndexChanged(int i){
    core.setRandomVectorCoordsId(i);
}

bool MainWindow::areXValuesValid(){
    QStringList list;
    QRegExp expr("^[0-9]+\\.*[0-9]*$");
    int count = 0;

    list = ui->lineEditXVals->text().trimmed().split(",");

    for(int i = 0; i < list.size(); i++){
        if(expr.exactMatch(list[i])){
            count++;
        }

        if(list[i].toDouble() == 0.){
            return false;
        }
    }

    if(count == list.size()){
        return true;
    }

    return false;

}
