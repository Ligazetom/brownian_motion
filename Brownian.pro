#-------------------------------------------------
#
# Project created by QtCreator 2019-02-01T14:53:12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Brownian
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    bmath.cpp \
    core.cpp \
    graph.cpp

HEADERS  += mainwindow.h \
    bmath.h \
    core.h \
    graph.h

FORMS    += mainwindow.ui

CONFIG += c++11
