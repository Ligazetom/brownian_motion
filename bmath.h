#ifndef BMATH_H
#define BMATH_H
#define CLT_AMOUNT_TO_GENERATE 100
#define MIN_GEN_VAL -1
#define MAX_GEN_VAL 1

#include <cstdlib>
#include <cmath>

namespace BMath
{
    double generateNormalDistributedNumber();
};

#endif // BMATH_H
