#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#define IMAGE_MARGIN 10

#include <QMainWindow>
#include "qdebug.h"
#include "graph.h"
#include "core.h"
#include "qstring.h"
#include "qstringlist.h"
#include "qmessagebox.h"
#include "qregexp.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    Graph graph;
    Core core;
    bool shouldCreateNew[3];
    QMessageBox msg;

    std::vector<double> getXValues();
    void generateXValues();
    bool isVectorImageWanted();
    bool areXValuesValid();

private slots:
    void on_pushButtonGenerate_pressed();
    void on_spinBoxSteps_editingFinished();
    void on_lineEditXVals_editingFinished();
    void on_doubleSpinBoxEndpt_editingFinished();
    void on_comboBoxImage_currentIndexChanged(int i);
    void on_radioButtonTrajectory_toggled();
    void on_radioButtonVector_toggled();
    void on_checkBoxSuitable_toggled(bool b);
    void on_comboBoxCoords_currentIndexChanged(int i);
};

#endif // MAINWINDOW_H
